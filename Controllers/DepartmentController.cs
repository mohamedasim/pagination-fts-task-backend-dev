using System;
using System.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using StudentApiUsingSql.Models;

namespace StudentApiUsingSql.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class DepartmentController : Controller
    {
        IConfiguration Configure; 

         public DepartmentController (IConfiguration configuration){
             Configure=configuration;
         }
               [HttpGet("list")]
        public ActionResult Get(){
            string query= @"select * from Department";

            DataTable table = new DataTable();
            string sqlDatasource = Configure.GetConnectionString("DefaultConnection");
             SqlDataReader dataReader;
             using(SqlConnection connection = new SqlConnection(sqlDatasource)){
                 connection.Open();
                 using(SqlCommand cmd = new SqlCommand(query,connection)){
                     dataReader = cmd.ExecuteReader();
                     table.Load(dataReader);
                 }
             }
             
            return new JsonResult(table) ;
        }
        [HttpGet("single/{id}")]
        public ActionResult GetSingleById(int id){
             string query= @"select * from Department where Id ='"+id+"'";

            DataTable table = new DataTable();
            string sqlDatasource = Configure.GetConnectionString("DefaultConnection");
             SqlDataReader dataReader;
             using(SqlConnection connection = new SqlConnection(sqlDatasource)){
                 connection.Open();
                 using(SqlCommand cmd = new SqlCommand(query,connection)){
                     dataReader = cmd.ExecuteReader();
                     table.Load(dataReader);
                 }
             }
              return new JsonResult(table) ;
        }

        [HttpPost("create")]
        public ActionResult Post(DepartmentModel department){
             string query= @"insert into  Department values('"+department.Name+ @"')";

            DataTable table = new DataTable();
            string sqlDatasource = Configure.GetConnectionString("DefaultConnection");
             SqlDataReader dataReader;
             using(SqlConnection connection = new SqlConnection(sqlDatasource)){
                 connection.Open();
                 using(SqlCommand cmd = new SqlCommand(query,connection)){
                     dataReader = cmd.ExecuteReader();
                     table.Load(dataReader);
                 }
             }
              return new JsonResult("your Data Created Successfully!") ;
        }

        [HttpPut("update/{id}")]
        public ActionResult Put(DepartmentModel department,int id){
             string query= @"update Department set Name ='"+department.Name+ @"'where Id='"+id+"'";

            DataTable table = new DataTable();
            string sqlDatasource = Configure.GetConnectionString("DefaultConnection");
             SqlDataReader dataReader;
             using(SqlConnection connection = new SqlConnection(sqlDatasource)){
                 connection.Open();
                 using(SqlCommand cmd = new SqlCommand(query,connection)){
                     dataReader = cmd.ExecuteReader();
                     table.Load(dataReader);
                 }
             }
              return new JsonResult("your Data Updated Successfully!") ;
        }

        [HttpDelete("delete/{id}")]
        public ActionResult Delete(int id){
             string query= @"delete from Department where Id='"+id+@"'";

            DataTable table = new DataTable();
            string sqlDatasource = Configure.GetConnectionString("DefaultConnection");
             SqlDataReader dataReader;
             using(SqlConnection connection = new SqlConnection(sqlDatasource)){
                 connection.Open();
                 using(SqlCommand cmd = new SqlCommand(query,connection)){
                     dataReader = cmd.ExecuteReader();
                     table.Load(dataReader);
                 }
             }
              return new JsonResult("your Requested Data has been deleted Successfully!") ;
        }
    }
}