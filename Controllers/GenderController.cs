using System;
using System.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using StudentApiUsingSql.Models;
namespace StudentApiUsingSql.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class GenderController : Controller
    {
        IConfiguration Configure; 

         public GenderController (IConfiguration configuration){
             Configure=configuration;
         }
        [HttpGet("list")]
        public ActionResult Get(){
            string query= @"select * from Gender";

            DataTable table = new DataTable();
            string sqlDatasource = Configure.GetConnectionString("DefaultConnection");
             SqlDataReader dataReader;
             using(SqlConnection connection = new SqlConnection(sqlDatasource)){
                 connection.Open();
                 using(SqlCommand cmd = new SqlCommand(query,connection)){
                     dataReader = cmd.ExecuteReader();
                     table.Load(dataReader);
                 }
             }
             
            return new JsonResult(table) ;
        }
        [HttpGet("single/{id}")]
        public ActionResult GetSingleById(int id){
             string query= @"select * from Gender where Id ='"+id+"'";

            DataTable table = new DataTable();
            string sqlDatasource = Configure.GetConnectionString("DefaultConnection");
             SqlDataReader dataReader;
             using(SqlConnection connection = new SqlConnection(sqlDatasource)){
                 connection.Open();
                 using(SqlCommand cmd = new SqlCommand(query,connection)){
                     dataReader = cmd.ExecuteReader();
                     table.Load(dataReader);
                 }
             }
              return new JsonResult(table) ;
        }

        [HttpPost("create")]
        public ActionResult Post(GenderModel gender){
             string query= @"insert into  Gender values('"+gender.Name+ @"')";

            DataTable table = new DataTable();
            string sqlDatasource = Configure.GetConnectionString("DefaultConnection");
             SqlDataReader dataReader;
             using(SqlConnection connection = new SqlConnection(sqlDatasource)){
                 connection.Open();
                 using(SqlCommand cmd = new SqlCommand(query,connection)){
                     dataReader = cmd.ExecuteReader();
                     table.Load(dataReader);
                 }
             }
              return new JsonResult("your Data Created Successfully!") ;
        }

        [HttpPut("update/{id}")]
        public ActionResult Put(GenderModel gender,int id){
             string query= @"update Gender set Name ='"+gender.Name+ @"'where Id='"+id+"'";

            DataTable table = new DataTable();
            string sqlDatasource = Configure.GetConnectionString("DefaultConnection");
             SqlDataReader dataReader;
             using(SqlConnection connection = new SqlConnection(sqlDatasource)){
                 connection.Open();
                 using(SqlCommand cmd = new SqlCommand(query,connection)){
                     dataReader = cmd.ExecuteReader();
                     table.Load(dataReader);
                 }
             }
              return new JsonResult("your Data Updated Successfully!") ;
        }

        [HttpDelete("delete/{id}")]
        public ActionResult Delete(int id){
             string query= @"delete from Gender where Id='"+id+@"'";

            DataTable table = new DataTable();
            string sqlDatasource = Configure.GetConnectionString("DefaultConnection");
             SqlDataReader dataReader;
             using(SqlConnection connection = new SqlConnection(sqlDatasource)){
                 connection.Open();
                 using(SqlCommand cmd = new SqlCommand(query,connection)){
                     dataReader = cmd.ExecuteReader();
                     table.Load(dataReader);
                 }
             }
              return new JsonResult("your Requested Data has been deleted Successfully!") ;
        }
    }
}